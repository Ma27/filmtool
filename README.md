# filmtool

[![pipeline status](https://img.shields.io/endpoint?url=https://hydra.ist.nicht-so.sexy/job/filmtool/master/release.x86_64-linux/shield)](https://hydra.ist.nicht-so.sexy/job/filmtool/master/release.x86_64-linux/)

Keep track of movies to be watched.
