# Backend

* [x] Film ranking ändern
* [x] Film als geschaut markieren
* [x] Nur geschaute/anzuschauende Filme via API holen
  * [x] Sortieren nach Ranking
* [x] imdb Search Proxy
  * [x] Cache
* [x] Vorschaubilder
* [x] Hot reload
* [x] Delete covers
* [x] Stream Wo?
* [x] Sprache in imdb Suche konfigurierbar machen
* [x] chmod imgs
* [x] Img API
* [x] UNDO boxes on each page
* [x] Korrekte Rückgabe von Informationen bei `POST /movies`
* [x] ListItemButton AddNew fixen
* [x] Multiple Deletes
* [x] Check if Cover exists
* [x] Logging
* [ ] Plex
* [ ] Refresh Feature

# Dev Env

* [ ] Farben / Logging
* [x] Mehr make
* [ ] Doku

# Build

* [x] Production build
* [x] Tests
* [x] ESLint

# Frontend

* [x] Basic setup
* [x] Frontend Spike
* [x] Suche
* [x] Hinzufügen / Entfernen / Favoriten / Watched
  * [x] Undo
  * [x] Confirm Deletion
* [x] Error handling
* [x] Loading spinner
* [x] Undo
* [x] Show age
* [x] Seen notification
* [x] Resolve all warnings
* [x] Doppel-UNDO Bug
* [x] Suche von Scheduling
* [x] deduplicate reducer stmts
* [x] filter/find generalisieren
* [x] fade out notifications

# v2

* [ ] Better propTypes
* [ ] Archiv
* [x] Multiple UNDO boxes
* [ ] Suche: Bilder
* [ ] Suche: ignore already added
* [x] Prüfen, ob DB up-to-date
* [ ] Krassere Suche
* [ ] Pagination
  * Code:
    ```
    <Stack spacing={2}>
      <Pagination count={10} color="primary" />
    </Stack>
    ```
* [ ] Trash bin
* [ ] Refresh Abos
* [x] Disney+
* [ ] Payment Options
* [ ] Mobile
* [x] Keine Absoluten bildpfade
