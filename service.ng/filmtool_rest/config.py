from configparser import ConfigParser, SectionProxy
from dataclasses import dataclass
from os import environ, getlogin
from typing import Dict, Union


@dataclass
class General:
    log_level: str
    cover_base_dir: str
    cors_dev: bool


@dataclass
class DB:
    params: Dict[str, Union[int, str]]


@dataclass
class TMDB:
    lang: str
    api_token: str
    provider_availability_region: str


@dataclass
class ServiceConfig:
    general: General
    db: DB
    tmdb: TMDB


def read_db(params: SectionProxy) -> DB:
    if "password_file" in params:
        with open(params["password_file"], "r", encoding="utf-8") as f:
            params["password"] = f.read().rstrip()
            del params["password_file"]

    assert "dbname" in params, "Key dbname must be present in [db] section!"
    allowed = ["host", "password", "port", "user", "dbname"]
    if "user" not in params:
        params["user"] = getlogin()
    return DB(params={k: v for k, v in params.items() if k in allowed})


def read_general(sect: SectionProxy) -> General:
    assert "cover_base_dir" in sect, "Section [general] expects field cover_base_dir!"
    return General(
        log_level=sect.get("log_level", "warning").upper(),
        cover_base_dir=str(sect["cover_base_dir"]),
        cors_dev=sect.getboolean("cors_dev", fallback=False),
    )


def read_tmdb(sect: SectionProxy) -> TMDB:
    assert "api_token_file" in sect, "Section [tmdb] expects field api_token_file!"
    with open(sect["api_token_file"], "r", encoding="utf-8") as f:
        api_token = f.read().rstrip()
    return TMDB(
        lang=sect.get("lang", "en-US"),
        api_token=api_token,
        provider_availability_region=sect.get("provider_availability_region", "DE"),
    )


def get_section(cfg: ConfigParser, name: str, path: str) -> SectionProxy:
    assert name in cfg, f"Missing [{name}] section in file {path}!"
    return cfg[name]


def read_config(path: str) -> ServiceConfig:
    allowed_keys = [
        "HOME",
        "STATE_DIRECTORY",
        "CACHE_DIRECTORY",
        "CREDENTIALS_DIRECTORY",
        "RUNTIME_DIRECTORY",
    ]

    cfg = ConfigParser({k: v for k, v in environ.items() if k in allowed_keys})
    cfg.read(path)

    return ServiceConfig(
        general=read_general(get_section(cfg, "general", path)),
        db=read_db(get_section(cfg, "db", path)),
        tmdb=read_tmdb(get_section(cfg, "tmdb", path)),
    )


def read_config_from_env() -> ServiceConfig:
    assert "FILMTOOL_REST_CFG_FILE" in environ
    return read_config(environ["FILMTOOL_REST_CFG_FILE"])
