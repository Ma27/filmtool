from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from typing import List

from .db import DBConnectionHandle


class Rank(int, Enum):
    RED = 1
    YELLOW = 2
    BLUE = 3


class Provider(str, Enum):
    NETFLIX = "netflix"
    DISNEYPLUS = "disneyplus"
    AMAZONPRIME = "prime"

    @staticmethod
    def from_tmdb_id(tmdb_id):
        match tmdb_id:
            case 8:
                return Provider.NETFLIX
            case 9:
                return Provider.AMAZONPRIME
            case 337:
                return Provider.DISNEYPLUS


@dataclass
class StreamingProviderAvailability:
    link: str
    provider: Provider


@dataclass
class Movie:
    id: int
    title: str
    description: str
    imdb_id: str
    tmdb_id: int
    added: datetime
    watched: datetime | None
    rank: Rank
    deleted_at: datetime | None
    availability: List[StreamingProviderAvailability]


class InvalidMovieID(Exception):
    def __init__(self, movie_id: int):
        self.movie_id = movie_id


class MovieAlreadyExists(Exception):
    def __init__(self, tmdb_id: int):
        self.tmdb_id = tmdb_id


class MovieRepository:
    def __init__(self, db_handle: DBConnectionHandle):
        self.db_handle: DBConnectionHandle = db_handle

    def add(self, movie: Movie) -> Movie:
        with self.db_handle() as db:
            with db.cursor() as cur:
                cur.execute(
                    "SELECT deleted_at, rank FROM movie WHERE tmdb_id = %s",
                    (movie.tmdb_id,),
                )
                if (row := cur.fetchone()) is not None:
                    if row[0] is None:
                        raise MovieAlreadyExists(movie.tmdb_id)

                    movie.rank = Rank(int(row[1]))
                    cur.execute(
                        """
                        UPDATE movie
                        SET deleted_at = NULL, watched = NULL
                        WHERE tmdb_id = %s
                        RETURNING id
                        """,
                        (movie.tmdb_id,),
                    )
                    assert (row := cur.fetchone()) is not None
                    movie.id = row[0]
                else:
                    cur.execute(
                        """
                        INSERT INTO movie (
                            title,
                            description,
                            imdb_id,
                            tmdb_id,
                            added,
                            rank
                        )
                        VALUES (%s, %s, %s, %s, %s, %s)
                        RETURNING id
                        """,
                        (
                            movie.title,
                            movie.description,
                            movie.imdb_id,
                            movie.tmdb_id,
                            movie.added,
                            str(movie.rank.value),
                        ),
                    )

                    assert (row := cur.fetchone()) is not None
                    movie.id = row[0]

                    for avail in movie.availability:
                        cur.execute(
                            """
                            INSERT INTO streaming_provider (movie_id, provider, link)
                            VALUES(%s, %s, %s)
                            """,
                            (movie.id, avail.provider.value, avail.link),
                        )

        return movie

    def get_unwatched(self, limit: int, offset: int) -> List[Movie]:
        with self.db_handle.cursor() as cur:
            result: List[Movie] = []

            cur.execute(
                """
                SELECT
                    m.id,
                    m.title,
                    m.description,
                    m.imdb_id,
                    m.tmdb_id,
                    m.added,
                    m.rank,
                    s.provider,
                    s.link
                FROM movie m
                LEFT JOIN streaming_provider s ON m.id = s.movie_id
                INNER JOIN (SELECT id FROM movie LIMIT %s OFFSET %s) m2 ON m.id = m2.id
                WHERE m.deleted_at IS NULL
                  AND m.watched IS NULL
                ORDER BY m.rank ASC, m.added ASC
                """,
                (limit, offset),
            )

            prev_id = None
            for (
                movie_id,
                title,
                desc,
                imdb_id,
                tmdb_id,
                added,
                rank,
                provider,
                link,
            ) in cur.fetchall():
                if prev_id != movie_id:
                    movie = Movie(
                        movie_id,
                        title,
                        desc,
                        imdb_id,
                        tmdb_id,
                        added,
                        None,
                        Rank(int(rank)),
                        None,
                        [],
                    )
                    prev_id = movie_id

                    result.append(movie)

                if provider is not None:
                    movie.availability.append(
                        StreamingProviderAvailability(link, Provider(provider))
                    )

            return result

    def get_tmdb_by_id(self, movie_id: int) -> int | None:
        with self.db_handle.cursor() as cur:
            cur.execute(
                """
                SELECT m.tmdb_id FROM movie m where m.id = %s AND m.deleted_at IS NULL
                """,
                (movie_id,),
            )
            if (row := cur.fetchone()) is None:
                raise InvalidMovieID(movie_id)

            return row[0]

    def update_movie_watched(self, movie_id: int, watched: datetime | None):
        with self.db_handle() as db:
            with db.cursor() as cur:
                expect_watched = "NOT" if watched is None else ""
                cur.execute(
                    f"""
                    SELECT 1
                    FROM movie m
                    WHERE m.id = %s
                      AND m.deleted_at IS NULL
                      AND m.watched IS {expect_watched} NULL
                    """,
                    (movie_id,),
                )
                if cur.fetchone() is None:
                    raise InvalidMovieID(movie_id)

                cur.execute(
                    "UPDATE movie SET watched = %s WHERE id = %s", (watched, movie_id)
                )

    def update_rank(self, movie_id: int, rank: Rank):
        with self.db_handle() as db:
            with db.cursor() as cur:
                cur.execute(
                    """
                    SELECT 1
                    FROM movie m
                    WHERE m.id = %s
                      AND m.deleted_at IS NULL
                      AND m.watched IS NULL
                    """,
                    (movie_id,),
                )
                if cur.fetchone() is None:
                    raise InvalidMovieID(movie_id)

                cur.execute(
                    "UPDATE movie SET rank = %s WHERE id = %s RETURNING id",
                    (str(rank.value), movie_id),
                )

    def soft_delete_movie(self, movie_id: int):
        with self.db_handle() as db:
            with db.cursor() as cur:
                cur.execute(
                    "SELECT 1 FROM movie m WHERE m.id = %s AND m.deleted_at IS NULL",
                    (movie_id,),
                )
                if cur.fetchone() is None:
                    raise InvalidMovieID(movie_id)

                cur.execute(
                    "UPDATE movie SET deleted_at = %s WHERE id = %s",
                    (datetime.now().astimezone(), movie_id),
                )
