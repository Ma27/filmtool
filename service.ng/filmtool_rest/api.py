from dataclasses import dataclass
from datetime import datetime
from os import chmod, mkdir
from os.path import exists as path_exists
from os.path import splitext
from typing import List

import requests

from .config import TMDB
from .data import Movie, Provider, Rank, StreamingProviderAvailability


class InvalidMovieID(Exception):
    pass


@dataclass
class SearchResult:
    tmdb_id: int
    original_title: str
    link: str
    description: str


class TMDBConsumer:
    def __init__(self, tmdb_cfg: TMDB, cover_base_dir: str):
        self.tmdb_cfg = tmdb_cfg
        self.cover_base_dir = cover_base_dir
        if not path_exists(self.cover_base_dir):
            mkdir(self.cover_base_dir)

    def search(self, term: str) -> List[SearchResult]:
        resp = requests.get(
            "https://api.themoviedb.org/3/search/movie",
            params={"query": term, "lang": self.tmdb_cfg.lang},
            headers={"Authorization": f"Bearer {self.tmdb_cfg.api_token}"},
            timeout=2,
        )
        resp.raise_for_status()

        return [
            SearchResult(
                tmdb_id=item["id"],
                original_title=item["original_title"],
                link=f"https://www.themoviedb.org/movie/{item['id']}",
                description=item["overview"],
            )
            for item in resp.json().get("results", [])
        ]

    def get_by_id(self, tmdb_id: int, time_added: datetime) -> Movie:
        resp = requests.get(
            f"https://api.themoviedb.org/3/movie/{tmdb_id}",
            headers={"Authorization": f"Bearer {self.tmdb_cfg.api_token}"},
            timeout=2,
        )

        if resp.status_code == 404:
            raise InvalidMovieID()

        resp.raise_for_status()
        movie_base = resp.json()
        self.__download_cover(movie_base["poster_path"], tmdb_id)

        return Movie(
            -1,
            movie_base["original_title"],
            movie_base["overview"],
            movie_base["imdb_id"],
            tmdb_id=tmdb_id,
            added=time_added,
            watched=None,
            rank=Rank.BLUE,
            deleted_at=None,
            availability=self.__get_availability(tmdb_id),
        )

    def __download_cover(
        self, poster_path: str, tmdb_id: int, re_download: bool = False
    ) -> str:
        file_path = f"{self.cover_base_dir}/{tmdb_id}{splitext(poster_path)[-1]}"
        if not path_exists(file_path) or re_download:
            resp = requests.get(
                f"https://image.tmdb.org/t/p/w200{poster_path}",
                stream=True,
                timeout=2,
            )
            resp.raise_for_status()
            with open(file_path, "wb") as f:
                for ch in resp.iter_content(1024):
                    f.write(ch)

        chmod(file_path, 0o400)

        return file_path

    def __get_availability(self, tmdb_id: int) -> List[StreamingProviderAvailability]:
        resp = requests.get(
            f"https://api.themoviedb.org/3/movie/{tmdb_id}/watch/providers",
            headers={"Authorization": f"Bearer {self.tmdb_cfg.api_token}"},
            timeout=2,
        )
        resp.raise_for_status()

        results = resp.json()["results"]
        if self.tmdb_cfg.provider_availability_region not in results:
            return []

        data = results[self.tmdb_cfg.provider_availability_region]

        return [
            # FIXME nicer way?
            StreamingProviderAvailability(data["link"], provider)
            for x in data.get("flatrate", [])
            if (provider := Provider.from_tmdb_id(x["provider_id"])) is not None
        ]
