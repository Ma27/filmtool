from datetime import datetime
from logging import warning

from flask import Blueprint, current_app, jsonify, request, send_file
from psycopg2 import Error as DBError
from requests import RequestException

from .config import General
from .data import InvalidMovieID, MovieAlreadyExists, MovieRepository, Rank
from .error import (
    API_CALL_ERROR,
    DB_ERROR,
    INVALID_MOVIE_ID,
    INVALID_RANK,
    MISSING_REQUEST_ARGUMENT,
    MOVIE_ALREADY_EXISTS,
)

http = Blueprint("api", __name__, url_prefix="/")


@http.after_request
def cors_dev(response):
    cfg: General = current_app.extensions["filmtool_config"].general
    if cfg.cors_dev:
        response.headers["Access-Control-Allow-Origin"] = "*"
        response.headers["Access-Control-Allow-Headers"] = "*"
        response.headers["Access-Control-Allow-Methods"] = "*"
    return response


@http.errorhandler(DBError)
def handle_db_error(e: DBError):
    warning(f"DB error: {e}")
    return jsonify(
        {
            "code": DB_ERROR,
            "message": "Database operation error. Contact the system administrator.",
        }
    ), 500


@http.errorhandler(InvalidMovieID)
def handle_invalid_movie_id(e: InvalidMovieID):
    return jsonify(
        {
            "code": INVALID_MOVIE_ID,
            "message": f"Movie with id {e.movie_id} doesn't exist!",
        }
    ), 404


@http.errorhandler(RequestException)
def handle_tmdb_call(e: RequestException):
    warning(f"Calling TMDB failed: {e}")
    return jsonify(
        {
            "code": API_CALL_ERROR,
            "message": "Calling TMDB failed. Contact the system administrator.",
        }
    ), 503


@http.errorhandler(MovieAlreadyExists)
def handle_movie_already_exists(e: MovieAlreadyExists):
    return jsonify(
        {
            "code": MOVIE_ALREADY_EXISTS,
            "message": f"Movie with tmdb id {e.tmdb_id} already exists.",
        }
    ), 400


@http.route("/proxy/tmdb")
def tmdb_search():
    term = request.args.get("term")
    if not term:
        return jsonify(
            {"code": MISSING_REQUEST_ARGUMENT, "message": "Expected query param 'term'"}
        ), 400

    consumer = current_app.extensions["api_consumer"]

    return jsonify(consumer.search(term))


@http.route("/movies", methods=["post"])
def insert_movie():
    consumer = current_app.extensions["api_consumer"]
    repo: MovieRepository = current_app.extensions["movie_repo"]
    movie_id = request.json.get("api_movie_id")  # type: ignore
    if not movie_id:
        return jsonify(
            {
                "code": MISSING_REQUEST_ARGUMENT,
                "message": "Expected api_movie_id in request body",
            }
        ), 400

    return jsonify(
        repo.add(consumer.get_by_id(movie_id, datetime.now().astimezone()))
    ), 201


@http.route("/movies/<movie_id>", methods=["delete"])
def delete_movie(movie_id):
    repo: MovieRepository = current_app.extensions["movie_repo"]
    repo.soft_delete_movie(movie_id)
    return {}, 204


@http.route("/movies", methods=["get"])
def unwatched_movies():
    repo: MovieRepository = current_app.extensions["movie_repo"]

    return jsonify(
        repo.get_unwatched(
            int(request.args.get("limit", 200)), int(request.args.get("offset", 0))
        )
    )


@http.route("/movies/<movie_id>/image", methods=["get"])
def cover(movie_id):
    repo: MovieRepository = current_app.extensions["movie_repo"]
    cfg: General = current_app.extensions["filmtool_config"].general
    tmdb_id = repo.get_tmdb_by_id(movie_id)
    return send_file(f"{cfg.cover_base_dir}/{tmdb_id}.jpg", mimetype="image/jpeg")


@http.route("/movies/<movie_id>/seen", methods=["patch"])
def seen(movie_id):
    repo: MovieRepository = current_app.extensions["movie_repo"]
    watched = datetime.now().astimezone()
    repo.update_movie_watched(movie_id, watched)
    return jsonify({"id": movie_id, "watched": watched}), 200


@http.route("/movies/<movie_id>/unseen", methods=["patch"])
def unsee(movie_id):
    repo: MovieRepository = current_app.extensions["movie_repo"]
    repo.update_movie_watched(movie_id, None)
    return jsonify({"id": movie_id, "watched": None}), 200


@http.route("/movies/<movie_id>/ranking", methods=["patch"])
def rank(movie_id):
    repo: MovieRepository = current_app.extensions["movie_repo"]
    rank_num = int(request.json.get("rank"))  # type: ignore
    if not rank_num or rank_num < 1 or rank_num > 3:
        return jsonify(
            {
                "code": INVALID_RANK,
                "message": "Expected rank in JSON body to be between 1 and 3",
            }
        ), 400
    repo.update_rank(movie_id, Rank(rank_num))
    return jsonify({"id": movie_id, "rank": rank_num})
