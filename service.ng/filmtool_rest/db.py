from contextlib import contextmanager
from typing import Callable, Iterator, Optional

from psycopg2 import InterfaceError
from psycopg2.extensions import connection, cursor


class DBConnectionHandle:
    def __init__(self, conn_factory: Callable[[], connection]):
        self.conn_factory = conn_factory
        self.conn: Optional[connection] = None
        self.handle = None

    @contextmanager
    def __call__(self) -> Iterator[connection]:
        if self.conn is None:
            self.conn = self.conn_factory()

        assert self.conn is not None

        try:
            self.conn.__enter__()
        except InterfaceError:
            self.conn = self.conn_factory()
            self.conn.__enter__()

        try:
            yield self.conn
        except Exception as e:
            self.conn.__exit__(type(e), e, e.__traceback__)
            raise
        finally:
            self.conn.__exit__(None, None, None)

    @contextmanager
    def cursor(self) -> Iterator[cursor]:
        with self() as db:
            yield db.cursor()
