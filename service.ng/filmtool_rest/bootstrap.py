import dataclasses
from datetime import datetime
from json import JSONEncoder
from logging import basicConfig as setup_logger

import psycopg2
from flask import Flask
from flask.json.provider import DefaultJSONProvider

from .api import TMDBConsumer
from .config import ServiceConfig, read_config_from_env
from .data import MovieRepository
from .db import DBConnectionHandle
from .service import http


class ISO8601DateTimeEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        return super().default(o)


class CustomJSONProvider(DefaultJSONProvider):
    def dumps(self, obj, **kwargs):
        return super().dumps(obj, cls=ISO8601DateTimeEncoder)

    @staticmethod
    def default(obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        if dataclasses.is_dataclass(obj) and not isinstance(obj, type):
            return dataclasses.asdict(obj)
        return obj


def start() -> Flask:
    return start_internal(read_config_from_env())


def start_internal(cfg: ServiceConfig) -> Flask:
    app = Flask(__name__)
    setup_logger(level=cfg.general.log_level)

    app.json = CustomJSONProvider(app)

    app.extensions["filmtool_config"] = cfg
    app.extensions["api_consumer"] = TMDBConsumer(cfg.tmdb, cfg.general.cover_base_dir)
    app.extensions["db"] = DBConnectionHandle(
        lambda: psycopg2.connect(**cfg.db.params)  # type: ignore
    )
    app.extensions["movie_repo"] = MovieRepository(app.extensions["db"])

    app.register_blueprint(http)

    return app
