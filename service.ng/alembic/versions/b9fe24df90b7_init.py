"""init

Revision ID: b9fe24df90b7
Revises: init movie table
Create Date: 2024-01-06 23:56:40.624961

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "b9fe24df90b7"
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "streaming_provider",
        sa.Column("movie_id", sa.INTEGER),
        sa.Column(
            "provider",
            sa.Enum("disneyplus", "netflix", "prime", name="streaming_provider_name"),
            nullable=False,
        ),
        sa.Column("link", sa.TEXT, nullable=False),
    )
    op.create_table(
        "movie",
        sa.Column("id", sa.INTEGER, primary_key=True, autoincrement=True),
        sa.Column("title", sa.VARCHAR(250), nullable=False),
        sa.Column("description", sa.TEXT, nullable=False),
        sa.Column("imdb_id", sa.VARCHAR(25), nullable=False),
        sa.Column("tmdb_id", sa.INTEGER, nullable=False),
        sa.Column("added", sa.DateTime(timezone=True), nullable=False),
        sa.Column("watched", sa.DateTime(timezone=True)),
        sa.Column("rank", sa.Enum("1", "2", "3", name="movie_rank"), nullable=False),
        sa.Column("deleted_at", sa.DateTime(timezone=True)),
    )
    op.create_unique_constraint("uq_movie_imdb", "movie", ["imdb_id"])
    op.create_unique_constraint("uq_movie_tmdb", "movie", ["tmdb_id"])
    op.create_index("in_movie_rank", "movie", ["rank"])
    op.create_foreign_key(
        "fk_movie_id", "streaming_provider", "movie", ["movie_id"], ["id"]
    )


def downgrade() -> None:
    op.drop_table("streaming_provider")
    op.drop_table("movie")
