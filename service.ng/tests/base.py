from tempfile import TemporaryDirectory
from unittest import TestCase

from alembic.command import upgrade as alembic_upgrade
from alembic.config import Config as AlembicConfig
from sqlalchemy import URL
from testing.postgresql import Postgresql as TestPostgresql

from filmtool_rest.bootstrap import start_internal
from filmtool_rest.config import DB, TMDB, General, ServiceConfig


class AppTestCase(TestCase):
    def setUp(self):
        self.postgresql = TestPostgresql()
        self.tmp_dir = TemporaryDirectory()
        dsn = self.postgresql.dsn()
        dsn["dbname"] = dsn.pop("database")
        self.service_config = ServiceConfig(
            general=General(
                log_level="WARNING",
                cover_base_dir=self.tmp_dir.name,
                cors_dev=False,
            ),
            db=DB(params=dsn.copy()),
            tmdb=TMDB(
                lang="en-US", api_token="hunter2", provider_availability_region="de"
            ),
        )
        self.app = start_internal(self.service_config)

        dsn["username"] = dsn.pop("user")
        dsn["database"] = dsn.pop("dbname")

        config = AlembicConfig()
        config.set_main_option("script_location", "alembic")
        config.set_main_option(
            "sqlalchemy.url", URL.create("postgresql", **dsn).render_as_string(False)
        )
        config.set_main_option("filmtool.is_test", "yes")
        alembic_upgrade(config, "head")

    def tearDown(self):
        self.postgresql.stop()
        self.tmp_dir.cleanup()
