# ruff: noqa: E501

import json

from .base import AppTestCase


class TestRESTApiEmpty(AppTestCase):
    def test_list_movies_empty(self):
        client = self.app.test_client()
        response = client.get("/movies")
        assert response.status_code == 200
        assert len(response.json) == 0, "No movies should be in the DB initially!"


class TestRESTApiWithFixtures(AppTestCase):
    def setUp(self):
        super().setUp()
        db_handle = self.app.extensions["db"]
        with db_handle() as db:
            with db.cursor() as cur:
                cur.execute(
                    (
                        """
INSERT INTO public.movie (id, title, description, imdb_id, tmdb_id, added, watched, rank, deleted_at) VALUES (4, 'The Gentlemen', 'American expat Mickey Pearson has built a highly profitable marijuana empire in London. When word gets out that he’s looking to cash out of the business forever it triggers plots, schemes, bribery and blackmail in an attempt to steal his domain out from under him.', 'tt8367814', 522627, '2024-01-18 19:46:10.097192+00', NULL, '2', NULL);
INSERT INTO public.streaming_provider (movie_id, provider, link) VALUES (4, 'netflix', 'https://www.themoviedb.org/movie/522627-the-gentlemen/watch?locale=DE');
INSERT INTO public.streaming_provider (movie_id, provider, link) VALUES (4, 'prime', 'https://www.themoviedb.org/movie/522627-the-gentlemen/watch?locale=DE');
                """
                    )
                )

    def test_list_movies(self):
        client = self.app.test_client()
        response = client.get("/movies")
        assert response.status_code == 200
        assert len(response.json) == 1, "Exactly one movie should be available!"

        the_gentlemen = response.json[0]
        assert the_gentlemen["title"] == "The Gentlemen"
        assert the_gentlemen["tmdb_id"] == 522627
        assert len(the_gentlemen["availability"]) == 2

        providers = [x["provider"] for x in the_gentlemen["availability"]]
        assert "netflix" in providers
        assert "prime" in providers
        assert the_gentlemen["watched"] is None

    def test_mark_as_watched(self):
        client = self.app.test_client()
        assert client.patch("/movies/4/seen").status_code == 200
        assert len(client.get("/movies").json) == 0, "No unwatched movies"
        assert client.patch("/movies/4/unseen").status_code == 200

        movies = client.get("/movies")
        assert len(movies.json) == 1
        assert movies.json[0]["watched"] is None

        seen_invalid_movie = client.patch("/movies/5/seen")
        assert seen_invalid_movie.status_code == 404, "No movie with ID 5 exists"
        assert seen_invalid_movie.json["code"] == 4

        unseen_invalid_movie = client.patch("/movies/5/unseen")
        assert unseen_invalid_movie.status_code == 404, "No movie with ID 5 exists"
        assert unseen_invalid_movie.json["code"] == 4

    def test_update_rank(self):
        client = self.app.test_client()
        assert client.get("/movies").json[0]["rank"] == 2

        invalid_rank = client.patch(
            "/movies/4/ranking",
            data=json.dumps({"rank": 4}),
            content_type="application/json",
        )
        assert invalid_rank.status_code == 400
        assert invalid_rank.json["code"] == 5

        assert (
            client.patch(
                "/movies/4/ranking",
                data=json.dumps({"rank": 1}),
                content_type="application/json",
            ).status_code
            == 200
        )

        movies = client.get("/movies")
        assert len(movies.json) == 1
        assert movies.json[0]["title"] == "The Gentlemen"
        assert movies.json[0]["rank"] == 1

    def test_pagination(self):
        db_handle = self.app.extensions["db"]
        with db_handle() as db:
            with db.cursor() as cur:
                cur.execute(
                    (
                        """
                INSERT INTO public.movie (id, title, description, imdb_id, tmdb_id, added, watched, rank, deleted_at) VALUES (9, 'Django Unchained', 'With the help of a German bounty hunter, a freed slave sets out to rescue his wife from a brutal Mississippi plantation owner.', 'tt1853728', 68718, '2024-01-18 19:47:31.365728+00', NULL, '3', NULL);
                """
                    )
                )

        client = self.app.test_client()
        assert len(client.get("/movies").json) == 2, "There are two unwatched movies"

        only_first_unwatched = client.get("/movies?limit=1").json
        assert len(only_first_unwatched) == 1
        assert only_first_unwatched[0]["title"] == "The Gentlemen"
        assert len(only_first_unwatched[0]["availability"]) == 2

        assert len(client.get("/movies?limit=0").json) == 0

        second_unwatched = client.get("/movies?offset=1&limit=1").json
        assert len(second_unwatched) == 1
        assert second_unwatched[0]["title"] == "Django Unchained"
        assert len(second_unwatched[0]["availability"]) == 0
