import json
from unittest import mock

from requests import HTTPError

from .base import AppTestCase


def successful_search_mock(*args, **kwargs):
    class MonkeyPatchResponse:
        def raise_for_status(self):
            pass

        def json(self):
            return {
                "results": [
                    {
                        "id": 68718,
                        "original_title": "Django Unchained",
                        "overview": "With the help of a German bounty hunter, a freed slave sets out to rescue his wife from a brutal Mississippi plantation owner.",  # noqa
                    }
                ]
            }

    return MonkeyPatchResponse()


def failing_search_mock(*args, **kwargs):
    class MonkeyPatchResponse:
        def raise_for_status(self):
            raise HTTPError("error 404")

        def json(self):
            pass

    return MonkeyPatchResponse()


def add_movie_mock(url, *args, **kwargs):
    if url == "https://api.themoviedb.org/3/movie/205321":

        class MovieDetailsResponse:
            status_code = 200

            def raise_for_status(self):
                pass

            def json(self):
                return {
                    "poster_path": "/img.jpg",
                    "original_title": "Sharknado",
                    "overview": "A freak hurricane hits Los Angeles, causing man-eating sharks to be scooped up in tornadoes and flooding the city with shark-infested seawater. Surfer and bar-owner Fin sets out with his friends Baz and Nova to rescue his estranged wife April and teenage daughter Claudia",  # noqa
                    "imdb_id": "tt2724064",
                }

        return MovieDetailsResponse()
    elif url == "https://image.tmdb.org/t/p/w200/img.jpg":

        class MovieCoverResponse:
            def raise_for_status(self):
                pass

            def iter_content(self, _):
                return [b"JPG mock"]

        return MovieCoverResponse()
    elif url == "https://api.themoviedb.org/3/movie/205321/watch/providers":

        class MovieWatchProvidersResponse:
            def raise_for_status(self):
                pass

            def json(self):
                return {
                    "results": {
                        "de": {
                            "flatrate": [{"provider_id": 8}],
                            "link": "https://example.com/",
                        }
                    }
                }

        return MovieWatchProvidersResponse()
    else:
        assert False, f"Unexpected URL: {url}"


class TestTMDB(AppTestCase):
    def test_search_invalid(self):
        client = self.app.test_client()
        assert client.get("/proxy/tmdb").status_code == 400

    @mock.patch("filmtool_rest.api.requests.get", side_effect=successful_search_mock)
    def test_search(self, http_mock):
        client = self.app.test_client()
        resp = client.get("/proxy/tmdb?term=django")

        assert resp.status_code == 200
        assert len(resp.json) == 1
        django_unchained = resp.json[0]
        assert django_unchained["original_title"] == "Django Unchained"

        assert len(http_mock.call_args_list) == 1

    @mock.patch("filmtool_rest.api.requests.get", side_effect=failing_search_mock)
    def test_api_call_error(self, _):
        client = self.app.test_client()
        resp = client.get("/proxy/tmdb?term=django")

        assert resp.status_code == 503
        assert resp.json["code"] == 2
        assert (
            resp.json["message"]
            == "Calling TMDB failed. Contact the system administrator."
        )

    def test_add_movie_invalid(self):
        client = self.app.test_client()
        resp = client.post("/movies", content_type="application/json", data="{}")
        assert resp.status_code == 400
        assert resp.json["code"] == 1

    @mock.patch("filmtool_rest.api.requests.get", side_effect=add_movie_mock)
    def test_add_movie(self, http_mock):
        client = self.app.test_client()
        resp = client.post(
            "/movies",
            content_type="application/json",
            data=json.dumps({"api_movie_id": 205321}),
        )

        assert resp.status_code == 201
        movie = resp.json

        assert movie["title"] == "Sharknado"
        assert movie["imdb_id"] == "tt2724064"
        assert movie["tmdb_id"] == 205321
        assert movie["deleted_at"] is None
        assert movie["rank"] == 3
        assert len(movie["availability"]) == 1
        assert movie["availability"][0]["provider"] == "netflix"
        assert movie["availability"][0]["link"] == "https://example.com/"
        assert len(http_mock.call_args_list) == 3

        added = movie["added"]

        assert (
            client.patch(
                "/movies/1/ranking",
                data=json.dumps({"rank": 1}),
                content_type="application/json",
            ).status_code
            == 200
        )

        unwatched_movies = client.get("/movies")
        assert unwatched_movies.status_code == 200
        assert len(unwatched_movies.json) == 1
        assert unwatched_movies.json[0]["title"] == "Sharknado"

        cover_mock = client.get(f"/movies/{movie['id']}/image")
        assert cover_mock.status_code == 200
        assert cover_mock.text == "JPG mock"

        add_existing = client.post(
            "/movies",
            content_type="application/json",
            data=json.dumps({"api_movie_id": 205321}),
        )
        assert add_existing.status_code == 400
        assert add_existing.json["code"] == 3

        deletion = client.delete("/movies/1")
        assert deletion.status_code == 204

        assert len(client.get("/movies").json) == 0

        undo_del = client.post(
            "/movies",
            content_type="application/json",
            data=json.dumps({"api_movie_id": 205321}),
        )
        assert undo_del.status_code == 201

        # FIXME the wrong added & rank are returned on POST, but the next request
        # retrieving the stuff from the DB returns the correct information.
        movie = client.get("/movies").json[0]
        assert added == movie["added"], "Addition date is retained"
        assert movie["rank"] == 1, "Rank is retained"
        # three times on first request, two times on second addition, two times on undo.
        # FIXME don't bother the API when undoing a deletion.
        assert len(http_mock.call_args_list) == 7
        assert len(client.get("/movies").json) == 1
