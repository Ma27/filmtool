#!/bin/sh

# wait for postgresql to listen
while ! pg_isready -h $(pwd)/.state/postgres -p 64444; do sleep 1; done

createdb -h $(pwd)/.state/postgres -p 64444 filmtool

alembic upgrade head
exec flask --app filmtool_rest.bootstrap:start --debug run
