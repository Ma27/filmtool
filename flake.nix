{
  description = "Keep track of movies to be watched";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    pre-commit = {
      url = "github:cachix/git-hooks.nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-compat.follows = "flake-compat";
        gitignore.follows = "gitignore";
      };
    };

    flake-compat.url = "github:edolstra/flake-compat";
    systems.url = "github:nix-systems/default";
    gitignore = {
      url = "github:hercules-ci/gitignore.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, pre-commit, ... }: let
    nixpkgsWithOverlay = nixpkgs.legacyPackages.x86_64-linux.extend self.overlays.default;
    inherit (nixpkgs.lib) flip getAttr;
    mkPythonList = python3Packages: map (flip getAttr python3Packages);
    mkPythonLibs = flip mkPythonList [
      "flask"
      "psycopg2"
      "requests"
    ];
    mkPythonCheckDeps = flip mkPythonList [
      "alembic"
      "mypy"
      "pytest"
      "pytest-xdist"
      "testing-postgresql"
      "types-psycopg2"
      "types-requests"
    ];
  in {
    nixosModules.filmtool = { pkgs, lib, config, ... }: with lib;
      let
        cfg = config.ma27.filmtool;
        format = pkgs.formats.ini { };
        isLocalDB = config.services.postgresql.enable;
      in {
        options.ma27.filmtool = {
          service = {
            enable = mkEnableOption "filmtool service";
            settings = mkOption {
              type = types.submodule {
                freeformType = format.type;
                options = {
                  general = {
                    cover_base_dir = mkOption {
                      type = types.str;
                      default = "%(STATE_DIRECTORY)s/covers";
                      description = mdDoc ''
                        Directory where covers of movies are stored.
                      '';
                    };
                    cors_dev = mkOption {
                      type = types.bool;
                      default = false;
                      description = mdDoc ''
                        Turn off all CORS protections. For development purposes only!
                      '';
                    };
                  };
                  tmdb.api_token_file = mkOption {
                    type = types.str;
                    default = "%(CREDENTIALS_DIRECTORY)s/tmdb_token";
                    description = mdDoc ''
                      Path to the API token file for TMDB.
                    '';
                  };
                };
              };
            };
            gunicornWorkers = mkOption {
              default = 2;
              type = types.ints.positive;
              description = mdDoc ''
                Amount of gunicorn workers.
              '';
            };
            tmdbTokenPath = mkOption {
              type = types.nullOr types.str;
              default = null;
              description = mdDoc ''
                Path to the file containing the API token for TMDb.
                Will be passed to the service via `LoadCredential`.
              '';
            };
          };
          vhost = {
            enable = mkEnableOption "filmtool vhost";
            enableSSL = mkEnableOption "ssl for filmtool's reverse proxy";
            name = mkOption {
              type = types.str;
              example = "filme.example.com";
              description = mdDoc ''
                Hostname for the vhost configured with nginx.
              '';
            };
          };
          filmtoolPackages = mkOption {
            default = nixpkgsWithOverlay.filmtool;
            type = let type = types.attrsOf (types.oneOf [
              types.package
              (types.functionTo type)
            ]); in type;
            description = mdDoc ''
              Package-set with Python interpreter and frontend/backend packages
              of filmtool.
            '';
          };
          host = mkOption {
            default = "localhost";
            type = types.str;
            description = mdDoc ''
              Hostname where the service itself is running.
            '';
          };
          port = mkOption {
            type = types.port;
            default = 8080;
            description = mdDoc ''
              Port the service should listen on.
            '';
          };
        };
        config = mkMerge [
          (mkIf cfg.service.enable {
            environment.etc."filmtool.cfg".source = format.generate
              "filmtool.cfg"
              cfg.service.settings;
            systemd.services = mapAttrs (const (x@{ serviceConfig, environment ? { }, ... }: x // {
              environment = environment // {
                FILMTOOL_REST_CFG_FILE = "/etc/filmtool.cfg";
              };
              restartTriggers = [ config.environment.etc."filmtool.cfg".source ];
              serviceConfig = serviceConfig // {
                DynamicUser = true;
                User = "filmtool";
                LoadCredential = mkIf (cfg.service.tmdbTokenPath != null) [
                  "tmdb_token:${cfg.service.tmdbTokenPath}"
                ];

                CapabilityBoundingSet = [ "" ];
                LockPersonality = true;
                MemoryDenyWriteExecute = true;
                NoNewPrivileges = true;
                PrivateDevices = true;
                PrivateUsers = true;
                ProcSubset = "pid";
                ProtectClock = true;
                ProtectControlGroups = true;
                ProtectHome = true;
                ProtectHostname = true;
                ProtectKernelModules = true;
                ProtectKernelLogs = true;
                ProtectKernelTunables = true;
                ProtectProc = "invisible";
                ProtectSystem = "strict";
                RestrictAddressFamilies = [ "AF_UNIX" "AF_INET" "AF_INET6" ];
                RestrictNamespaces = true;
                RestrictRealtime = true;
                RestrictSUIDSGID = true;
                RemoveIPC = true;
                StateDirectory = "filmtool";
                SystemCallArchitectures = "native";
                UMask = "0077";
              };
            })) {
              filmtool-alembic = {
                requires = mkIf isLocalDB [ "postgresql.service" ];
                after = mkIf isLocalDB [ "postgresql.service" ];
                description = "Alembic migration for filmtool";
                serviceConfig = {
                  Type = "oneshot";
                  ExecStart = "${cfg.filmtoolPackages.migrate}/bin/alembic upgrade head";
                  WorkingDirectory = lib.fileset.toSource {
                    root = ./service.ng;
                    fileset = lib.fileset.unions [
                      ./service.ng/alembic
                      ./service.ng/alembic.ini
                    ];
                  };
                };
              };
              filmtool = {
                requires = [ "filmtool-alembic.service" ];
                after = [ "filmtool-alembic.service" "network-online.target" ];
                wants = [ "network-online.target" ];
                wantedBy = [ "multi-user.target" ];
                description = "REST service for filmtool";
                environment = {
                  PYTHONPATH = "${cfg.filmtoolPackages.backend-runtime}/${cfg.filmtoolPackages.python.sitePackages}";
                };
                serviceConfig = {
                  Restart = "always";
                  RestartSec = "10";
                  WorkingDirectory = "/var/lib/private/filmtool";
                  ExecStart = concatStringsSep " " [
                    "${cfg.filmtoolPackages.backend-runtime.pkgs.gunicorn}/bin/gunicorn"
                    "'filmtool_rest.bootstrap:start()'"
                    "--name" "filmtool-rest"
                    "-u" "filmtool"
                    "-g" "filmtool"
                    "--workers" (toString cfg.service.gunicornWorkers)
                    "-k" "gevent"
                    "--bind" "${cfg.host}:${toString cfg.port}"
                  ];
                };
              };
            };
          })
          (mkIf cfg.vhost.enable {
            services.nginx = {
              enable = true;
              virtualHosts."${cfg.vhost.name}" = mkMerge [
                (mkIf cfg.vhost.enableSSL {
                  forceSSL = true;
                  enableACME = true;
                })
                {
                  root = "${cfg.filmtoolPackages.frontend}";
                  locations."/api/".extraConfig = ''
                    proxy_pass http://${cfg.host}:${toString cfg.port}/;
                  '';
                }
              ];
            };
          })
        ];
      };

    overlays.default = final: _prev: {
      filmtool = final.lib.makeExtensible (filmtoolFinal: {
        python = final.python3;

        gunicorn = filmtoolFinal.python.pkgs.gunicorn;

        backend-runtime = filmtoolFinal.python.buildEnv.override {
          extraLibs = [ filmtoolFinal.backend-ng filmtoolFinal.python.pkgs.gevent ];
        };

        backend-ng = filmtoolFinal.python.pkgs.buildPythonPackage {
          pname = "filmtool-restservice";
          src = final.lib.fileset.toSource {
            root = ./service.ng;
            fileset = final.lib.fileset.unions [
              ./service.ng/alembic
              ./service.ng/alembic.ini
              ./service.ng/pyproject.toml
              ./service.ng/filmtool_rest
              ./service.ng/Makefile
              ./service.ng/tests
            ];
          };
          version = "0.1.0";
          dependencies = mkPythonLibs filmtoolFinal.python.pkgs;
          pyproject = true;
          nativeBuildInputs = [ filmtoolFinal.python.pkgs.setuptools ];
          nativeCheckInputs = mkPythonCheckDeps filmtoolFinal.python.pkgs ++ [ final.ruff ];
          checkPhase = ''
            make lint
            make test
          '';
        };

        migrate = filmtoolFinal.python.pkgs.alembic.overrideAttrs
          ({ propagatedBuildInputs, ... }: {
            propagatedBuildInputs = propagatedBuildInputs ++ [
              filmtoolFinal.backend-ng
            ];
            doInstallCheck = false;
          });

        frontend = final.mkYarnPackage rec {
          name = "filmtool-frontend-${version}";
          inherit (builtins.fromJSON (builtins.readFile (self + "/ui/package.json"))) version;
          src = final.lib.fileset.toSource {
            root = ./ui;
            fileset = final.lib.fileset.unions [
              ./ui/package.json
              ./ui/yarn.lock
              ./ui/src
              ./ui/public
            ];
          };
          packageJSON = ./ui/package.json;
          yarnLock = ./ui/yarn.lock;
          yarnNix = ./ui/build/yarn.nix;
          NODE_ENV = "production";
          dontFixup = true;
          dontInstall = true;
          buildPhase = ''
            runHook preBuild

            export HOME=$(mktemp -d)
            pushd deps/ui &>/dev/null
              mv node_modules node_modules.bak
              cp -r $(readlink -f node_modules.bak) node_modules
              chmod +w node_modules
              yarn --offline run build
            popd &>/dev/null

            runHook postBuild
          '';
          distPhase = ''
            runHook preDist

            mkdir -p $out
            cp -r deps/ui/build/* $out

            runHook postDist
          '';
        };
      });
    };

    packages.x86_64-linux = {
      inherit (nixpkgsWithOverlay.filmtool)
        backend-ng
        frontend
        migrate
      ;
    };

    hydraJobs = let
      paths = builtins.attrNames self.packages.x86_64-linux;
    in {
      fmt-check = pre-commit.lib.x86_64-linux.run {
        src = ./.;
        hooks = {
          deadnix = {
            enable = true;
            excludes = [ "ui/build/yarn.nix" ];
          };
          biome = {
            enable = true;
            entry = "${nixpkgsWithOverlay.biome}/bin/biome lint --skip lint/suspicious/noArrayIndexKey --skip=lint/complexity/noUselessFragments";
            files = "\\.js$";
          };
          biome-format = {
            enable = true;
            entry = "${nixpkgsWithOverlay.biome}/bin/biome format --indent-style space --quote-style=single --write";
            files = "\\.js$";
          };
          ruff-check = {
            enable = true;
            entry = "${nixpkgsWithOverlay.ruff}/bin/ruff check";
            files = "\\.py$";
          };
          ruff-fmt = {
            enable = true;
            entry = "${nixpkgsWithOverlay.ruff}/bin/ruff format";
            files = "\\.py$";
          };
        };
      };
      integration-test.x86_64-linux = with nixpkgs.legacyPackages.x86_64-linux;
        nixosTest {
          name = "filmtool-integration";
          nodes.machine = {
            environment.systemPackages = [ jq ];
            imports = [ self.nixosModules.filmtool ];
            services.postgresql = {
              enable = true;
              initialScript = writeText "init.sql" ''
                create user filmtool;
                create database filmtool owner filmtool;
              '';
            };
            ma27.filmtool = {
              vhost = {
                enable = true;
                name = "machine";
              };
              service = {
                enable = true;
                tmdbTokenPath = "${writeText "demo" "hunter2"}";
                settings = {
                  general = {
                    log_level = "info";
                  };
                  db = {
                    user = "filmtool";
                    dbname = "filmtool";
                  };
                  tmdb = {
                    provider_availability_region = "DE";
                    lang = "de-DE";
                  };
                };
              };
            };
          };
          testScript = ''
            machine.start()

            machine.wait_for_unit("postgresql.service")
            machine.wait_for_unit("filmtool.service")
            machine.wait_for_open_port(8080)
            machine.wait_for_open_port(80)

            with subtest("Checking REST API"):
                assert 0 == int(machine.succeed("curl localhost/api/movies -f | jq '.|length'"))

            with subtest("Checking availability of frontend"):
                machine.succeed("curl -f localhost | grep '<title>Filmtool</title>'")

            machine.shutdown()
          '';
        };
      packages = nixpkgs.lib.genAttrs paths (name: {
        x86_64-linux = self.packages.x86_64-linux.${name};
      });
      release.x86_64-linux = with nixpkgs.legacyPackages.x86_64-linux;
        runCommand "filmtool-release"
          {
            _hydraAggregate = true;
            constituents = map (x: "packages.${x}.x86_64-linux") paths
              ++ [ "integration-test.x86_64-linux" ];
          }
          ''
            touch $out
          '';
    };

    devShell.x86_64-linux = with nixpkgs.legacyPackages.x86_64-linux;
      mkShell {
        name = "dev-env";
        buildInputs = [
          nodejs
          yarn
          yarn2nix
          nodePackages.typescript-language-server
          biome

          postgresql_15
          foreman
          ruff
          (python3.withPackages (ps: mkPythonLibs ps ++ mkPythonCheckDeps ps ++ (with ps; [
            python-lsp-server
            python-lsp-ruff
          ])))
        ];

        shellHook = self.hydraJobs.fmt-check.shellHook + ''
          export FILMTOOL_REST_CFG_FILE="$(git rev-parse --show-toplevel)/service.ng/develop.cfg"
          export STATE_DIRECTORY="$(git rev-parse --show-toplevel)/service.ng/.state"
          mkdir -p "$STATE_DIRECTORY"/covers

          token_path="$(git rev-parse --show-toplevel)"/tmdb_token
          if [ ! -f "$token_path" ]; then
            touch "$token_path"
            echo "Please write the tmdb_token to $token_path"
          else
            export TMDB_TOKEN="$(<"$token_path")"
          fi

          export NODE_ENV=development
        '';
      };
  };
}
