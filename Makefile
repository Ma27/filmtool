.PHONY: yarn2nix
yarn2nix:
	$(MAKE) -C ui yarn2nix

.PHONY: ui-dev
ui-dev:
	$(MAKE) -C ui dev

.PHONY: rest-dev
rest-dev:
	$(MAKE) -C service.ng dev

MAKEFLAGS := --jobs=2
.PHONY: dev
dev: rest-dev ui-dev

.PHONY: fmt
lint: ui-lint rest-lint

.PHONY: ui-lint
ui-lint:
	$(MAKE) -C ui lint

.PHONY: rest-lint
rest-lint:
	$(MAKE) -C service.ng lint

.PHONY: rest-test
rest-test:
	$(MAKE) -C service.ng test

.PHONY: test
test: rest-test

.PHONY: psql
psql:
	psql -h localhost -p 64444 -d filmtool

.PHONY: upgrade
upgrade:
	$(MAKE) -C ui upgrade

.PHONY: fmt
fmt: fmt-ui fmt-service

.PHONY: fmt-ui
fmt-ui:
	$(MAKE) -C ui fmt

.PHONY: fmt-service
fmt-service:
	$(MAKE) -C service.ng fmt
