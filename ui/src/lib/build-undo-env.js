import * as React from 'react';

import CloseIcon from '@mui/icons-material/Close';
import IconButton from '@mui/material/IconButton';
import Button from '@mui/material/Button';

export default function buildUndoEnv(key, onClose, onClick) {
  return {
    key: `snackbar-${key}`,
    action: (key) => (
      <React.Fragment>
        <Button color="secondary" size="small" onClick={() => onClick(key)}>
          UNDO
        </Button>
        <IconButton
          size="small"
          aria-label="close"
          color="inherit"
          onClick={() => onClose(key)}
        >
          <CloseIcon fontSize="small" />
        </IconButton>
      </React.Fragment>
    ),
  };
}
