export default function rankToColor(rank) {
  switch (Number.parseInt(rank, 10)) {
    case 1:
      return '#ff7066';
    case 2:
      return '#ff9a14';
    case 3:
      return '#5297ff';
    default:
      throw 'Invalid rank!';
  }
}
