import * as React from 'react';

export default function optionalComponent(cond, component) {
  return (
    <React.Fragment>
      {cond
        ? // WHY can't you be lazy like a normal language JS :(
          typeof component === 'function'
          ? component()
          : component
        : null}
    </React.Fragment>
  );
}
