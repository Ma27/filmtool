import { combineReducers } from 'redux';
import {
  SEARCH_MOVIES,
  EMPTY_SEARCH_TERM,
  FETCH_ERROR_SEARCH,
  FETCHING_SEARCH,
  FETCHING_TRACKED,
  OVERVIEW,
  UPDATE_SINGLE,
  UPDATE_RANK,
  SEEN,
  UNSEE,
  ADD_MOVIE,
  RM_MOVIE,
  UNDO_ADD_MOVIE,
  INIT,
  FETCH_ERROR_OVERVIEW,
  FETCH_ERROR_BOTH,
  FILTER_MOVIES,
} from '../actions';

const filterState = (data, predicate) => [
  (data || []).filter(predicate),
  (data || []).find((x) => !predicate(x)) || null,
];

const resetSearch = {
  errorMessage: null,
  fetching: false,
  recentlyAdded: null,
};

const search = (
  state = {
    ...resetSearch,
    result: [],
    noSearchPerformed: true,
  },
  action = { type: INIT },
) => {
  switch (action.type) {
    case FETCHING_SEARCH:
      return {
        ...state,
        ...resetSearch,
        fetching: true,
        noSearchPerformed: false,
      };
    case EMPTY_SEARCH_TERM:
      return {
        ...state,
        ...resetSearch,
        errorMessage: 'Empty search term!',
        result: [],
      };
    case SEARCH_MOVIES:
      return {
        ...state,
        ...resetOverview,
        result: action.json,
      };
    case FETCH_ERROR_BOTH:
    case FETCH_ERROR_SEARCH:
      return {
        ...state,
        ...resetSearch,
        noSearchPerformed: false,
        errorMessage: `Fetch error: ${action.e.message}`,
        result: [],
      };
    case UNDO_ADD_MOVIE:
      return {
        ...state,
        ...resetOverview,
        noSearchPerformed: false,
        result: [action.movieData].concat(state.result || []),
      };
    case ADD_MOVIE: {
      const [newState, recent] = filterState(
        state.result,
        ({ id }) => id !== action.data.id,
      );
      return {
        ...state,
        ...resetOverview,
        noSearchPerformed: false,
        result: newState,
        recentlyAdded: recent,
      };
    }
    default:
      return state;
  }
};

const mapItems = (result, id, changeSet) => {
  return (result || []).map((item) => {
    if (item.id !== id) {
      return item;
    }
    return Object.assign({}, item, changeSet);
  });
};

const resetOverview = {
  error: null,
  seenMovie: null,
  deletedMovie: null,
  fetching: false,
};

const addExtraFields = (data) => ({
  ...data,
  updating: false,
  show: true,
});

const overview = (
  state = {
    result: [],
    ...resetOverview,
  },
  action = { type: INIT },
) => {
  const genericFilter = (toFilter) =>
    filterState(toFilter, ({ id }) => id !== action.id);

  const prependData = (data) =>
    [addExtraFields(data)].concat(state.result || []);

  switch (action.type) {
    case FETCHING_TRACKED:
      return {
        ...state,
        ...resetOverview,
        fetching: true,
        result: state.result || [],
      };
    case OVERVIEW:
      return {
        ...state,
        ...resetOverview,
        result: action.json.map(addExtraFields),
      };
    case FETCH_ERROR_SEARCH:
      return {
        ...state,
        ...resetOverview,
      };
    case FETCH_ERROR_BOTH:
    case FETCH_ERROR_OVERVIEW:
      return {
        ...state,
        ...resetOverview,
        result: [],
        error: `Fetch error: ${action.e.message}`,
      };
    case UPDATE_SINGLE:
      return {
        ...state,
        ...resetOverview,
        result: mapItems(state.result, action.id, { updating: true }),
      };
    case UPDATE_RANK:
      return {
        ...state,
        ...resetOverview,
        result: mapItems(state.result, action.id, {
          updating: false,
          rank: Number.parseInt(action.rank, 10),
        }),
      };
    case SEEN: {
      const [newState, recent] = genericFilter(state.result);
      return {
        ...state,
        ...resetOverview,
        result: newState,
        seenMovie: recent,
      };
    }
    case UNSEE:
      return {
        ...state,
        ...resetOverview,
        result: prependData(action.data),
      };
    case ADD_MOVIE:
      return {
        ...state,
        ...resetOverview,
        result: prependData(action.data),
      };
    case RM_MOVIE:
    case UNDO_ADD_MOVIE: {
      const [newState, recent] = genericFilter(state.result);
      return {
        ...state,
        ...resetOverview,
        result: newState,
        deletedMovie: recent,
      };
    }
    case FILTER_MOVIES:
      return {
        ...state,
        ...resetOverview,
        result: (state.result || []).map((item) => ({
          ...item,
          show:
            item.title.toLowerCase().indexOf(action.term.toLowerCase()) !== -1,
        })),
      };
    default:
      return state;
  }
};

export default combineReducers({
  search,
  overview,
});
