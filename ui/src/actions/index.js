export const INIT = 'INIT';
export const SEARCH_MOVIES = 'SEARCH_MOVIES';
export const EMPTY_SEARCH_TERM = 'EMPTY_SEARCH_TERM';
export const FETCH_ERROR_SEARCH = 'FETCH_ERROR_SEARCH';
export const FETCH_ERROR_OVERVIEW = 'FETCH_ERROR_OVERVIEW';
export const FETCH_ERROR_BOTH = 'FETCH_ERROR_BOTH';
export const FETCHING_SEARCH = 'FETCHING_SEARCH';
export const FETCHING_TRACKED = 'FETCHING_TRACKED';
export const OVERVIEW = 'OVERVIEW';
export const UPDATE_RANK = 'UPDATE_RANK';
export const UPDATE_SINGLE = 'UPDATE_SINGLE';
export const SEEN = 'SEEN';
export const UNSEE = 'UNSEE';
export const ADD_MOVIE = 'ADD_MOVIE';
export const RM_MOVIE = 'RM_MOVIE';
export const UNDO_ADD_MOVIE = 'UNDO_ADD_MOVIE';
export const FILTER_MOVIES = 'FILTER_MOVIES';

export const filter = (term) => (dispatch) => {
  dispatch({
    type: FILTER_MOVIES,
    term,
  });
};

export const init = (dispatch) => {
  dispatch({
    type: INIT,
  });
};

let PREFIX;
if (process.env.NODE_ENV === 'development') {
  PREFIX = `http://localhost:${process.env.PORT || 5000}`;
} else {
  PREFIX = '/api';
}
const performRequest =
  (route, fetchError, dispatch, headers = {}, extra = {}) =>
  async (then) => {
    const x = fetch(`${PREFIX}/${route}`, {
      headers: {
        'Content-Type': 'application/json',
        ...headers,
      },
      ...extra,
    })
      .then((r) => {
        if (r.status >= 400) {
          return r.json().then((j) => {
            let message = `Code ${j.code}: ${j.message}`;
            if ('errors' in j) {
              message += ` (details: ${j.errors.join(', ')})`;
            }
            dispatch({
              type: fetchError,
              e: { message },
            });
            return Promise.reject({
              noError: true,
            });
          });
        }
        if (r.status !== 204) {
          return r.json();
        }
        return r;
      })
      .then(then)
      .catch((e) => {
        if (!('noError' in e)) {
          dispatch({
            type: fetchError,
            e,
          });
        }
      });
    return await x;
  };

export const fetchMovies = (term) => (dispatch) => {
  if (!term) {
    dispatch({
      type: EMPTY_SEARCH_TERM,
    });
  } else {
    dispatch({
      type: FETCHING_SEARCH,
    });
    performRequest(
      `proxy/tmdb?term=${encodeURIComponent(term)}`,
      FETCH_ERROR_SEARCH,
      dispatch,
    )((json) => {
      dispatch({
        type: SEARCH_MOVIES,
        json,
      });
    });
  }
};

export const fetchTracked = (dispatch) => {
  dispatch({
    type: FETCHING_TRACKED,
  });

  performRequest(
    'movies',
    FETCH_ERROR_OVERVIEW,
    dispatch,
  )((json) => {
    dispatch({
      type: OVERVIEW,
      json,
    });
  });
};

export const updateRank = (rank, id) => (dispatch) => {
  dispatch({
    type: UPDATE_SINGLE,
    id,
  });

  performRequest(
    `movies/${encodeURIComponent(id)}/ranking`,
    FETCH_ERROR_OVERVIEW,
    dispatch,
    {},
    {
      method: 'PATCH',
      body: JSON.stringify({ rank }),
    },
  )(() => {
    dispatch({
      type: UPDATE_RANK,
      id,
      rank,
    });
  });
};

export const seen = (id) => (dispatch) => {
  dispatch({
    type: UPDATE_SINGLE,
    id,
  });

  performRequest(
    `movies/${encodeURIComponent(id)}/seen`,
    FETCH_ERROR_OVERVIEW,
    dispatch,
    {},
    {
      method: 'PATCH',
    },
  )(() => {
    dispatch({
      type: SEEN,
      id,
    });
  });
};

export const unsee = (data) => (dispatch) => {
  performRequest(
    `movies/${encodeURIComponent(data.id)}/unseen`,
    FETCH_ERROR_OVERVIEW,
    dispatch,
    {},
    {
      method: 'PATCH',
    },
  )(() => {
    dispatch({
      type: UNSEE,
      data,
    });
  });
};

export const addMovie = (data) => (dispatch) => {
  dispatch({ type: FETCHING_SEARCH });
  dispatch({ type: FETCHING_TRACKED });

  performRequest(
    'movies',
    FETCH_ERROR_SEARCH,
    dispatch,
    {},
    {
      method: 'POST',
      body: JSON.stringify({
        api_movie_id: data.tmdb_id,
      }),
    },
  )((data) => {
    dispatch({
      type: ADD_MOVIE,
      data: {
        id: data.id,
        title: data.title,
        rank: data.rank || 3,
        description: data.description,
        watched: null,
        added: data.added,
        availability: data.availability,
      },
    });
  });
};

const doRmMovie = (id, dispatch) =>
  performRequest(
    `movies/${encodeURIComponent(id)}`,
    FETCH_ERROR_BOTH,
    dispatch,
    {},
    {
      method: 'DELETE',
    },
  );

export const rmMovie = (id) => (dispatch) => {
  dispatch({
    type: UPDATE_SINGLE,
    id,
  });

  doRmMovie(
    id,
    dispatch,
  )(() => {
    dispatch({
      type: RM_MOVIE,
      id,
    });
  });
};

export const undoAddMovie = (movieData) => (dispatch) => {
  dispatch({
    type: FETCHING_SEARCH,
  });
  doRmMovie(
    movieData.id,
    dispatch,
  )(() => {
    dispatch({
      type: UNDO_ADD_MOVIE,
      id: movieData.id,
      movieData,
    });
  });
};
