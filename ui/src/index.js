import { createRoot } from 'react-dom/client';
import App from './components/App';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@mui/material/styles';

import { red } from '@mui/material/colors';
import { createTheme } from '@mui/material/styles';

import { thunk } from 'redux-thunk';
import reducer from './reducers';

const middleware = [thunk];

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import { SnackbarProvider } from 'notistack';

import React from 'react';

const theme = createTheme({
  palette: {
    primary: {
      main: '#556cd6',
    },
    secondary: {
      main: '#19857b',
    },
    error: {
      main: red.A400,
    },
  },
});

const store = createStore(reducer, applyMiddleware(...middleware));

createRoot(document.querySelector('#content')).render(
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <SnackbarProvider maxSnack={3}>
      <Provider store={store}>
        <App />
      </Provider>
    </SnackbarProvider>
  </ThemeProvider>,
);
