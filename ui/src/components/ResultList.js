import * as React from 'react';
import PropTypes from 'prop-types';

import Alert from '@mui/material/Alert';
import CircularProgress from '@mui/material/CircularProgress';
import List from '@mui/material/List';
import Stack from '@mui/material/Stack';

export default function ResultList(props) {
  if (props.fetching) {
    return (
      <Stack alignItems="center" style={{ marginTop: 30 }}>
        <CircularProgress />
      </Stack>
    );
  }
  if (props.children.length === 0) {
    return (
      <Stack style={{ marginTop: 30, marginBottom: 15 }}>
        <Alert severity="warning">Sorry, nothing found</Alert>
      </Stack>
    );
  }
  return <List>{props.children}</List>;
}

ResultList.propTypes = {
  fetching: PropTypes.bool.isRequired,
  children: PropTypes.any,
};
