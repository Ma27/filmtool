import * as React from 'react';

import Alert from '@mui/material/Alert';

export default function Archive() {
  return (
    <React.Fragment>
      <h1>Trash bin</h1>
      <Alert style={{ marginBottom: 15 }} severity="warning">
        Deleted items will be automatically erased from the database after one
        week.
      </Alert>
      <Alert severity="info">Under construction</Alert>
    </React.Fragment>
  );
}
