import * as React from 'react';
import PropTypes from 'prop-types';

import optionalComponent from '../lib/optionalComponent.js';
import buildUndoEnv from '../lib/build-undo-env.js';

import SearchResultItem from './SearchResultItem.js';
import ResultList from './ResultList.js';

import Alert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import FormControl from '@mui/material/FormControl';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';

import { fetchMovies, addMovie, undoAddMovie, init } from '../actions';
import { connect } from 'react-redux';

import { enqueueSnackbar, closeSnackbar } from 'notistack';

class AddNew extends React.Component {
  static propTypes = {
    noSearchPerformed: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string,
    result: PropTypes.array.isRequired,
    fetching: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired,
    recentlyAdded: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.search = React.createRef();
  }

  componentDidMount() {
    this.props.dispatch(init);
  }

  handleSearch = () => {
    const searchTerm = this.search.current.value;
    this.props.dispatch(fetchMovies(searchTerm));
  };

  handleAdd = (data) => {
    this.props.dispatch(addMovie(data));
  };

  undoAdd = (data) => {
    this.props.dispatch(undoAddMovie(data));
  };

  componentDidUpdate(pProps) {
    if (pProps.recentlyAdded === null && this.props.recentlyAdded !== null) {
      const close = closeSnackbar;
      const undo = (key) => {
        close(key);
        this.undoAdd(this.props.recentlyAdded);
      };
      enqueueSnackbar(
        `Added movie ${this.props.recentlyAdded.title} to watchlist`,
        buildUndoEnv(`add-${this.props.recentlyAdded.id}`, close, undo),
      );
    }
  }

  render() {
    return (
      <Box component="form">
        <FormControl fullWidth style={{ marginTop: 10 }}>
          <Stack spacing={4} direction="row" sx={{ '& button': { m: 1 } }}>
            <TextField
              inputRef={this.search}
              fullWidth
              sx={{ m: 1 }}
              id="outlined-basic"
              label="Movie title"
              onKeyPress={(e) => {
                if (e.key === 'Enter') {
                  this.handleSearch.bind(this)();
                  e.preventDefault();
                  return false;
                }
              }}
              variant="outlined"
            />
            <Button
              onClick={this.handleSearch.bind(this)}
              variant="contained"
              style={{ height: 54, marginTop: 7 }}
            >
              Search
            </Button>
          </Stack>
        </FormControl>
        {optionalComponent(
          !this.props.noSearchPerformed && this.props.errorMessage !== null,
          <Alert severity="error">{this.props.errorMessage}</Alert>,
        )}
        {optionalComponent(
          !this.props.noSearchPerformed && this.props.errorMessage === null,
          <ResultList fetching={this.props.fetching}>
            {this.props.result.map((props, i) => (
              <SearchResultItem
                {...props}
                key={i}
                handleAdd={this.handleAdd.bind(this)}
              />
            ))}
          </ResultList>,
        )}
      </Box>
    );
  }
}

const mapStateToProps = (state) => {
  return state.search;
};

export default connect(mapStateToProps)(AddNew);
