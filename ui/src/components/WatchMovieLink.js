import * as React from 'react';
import PropTypes from 'prop-types';

import optionalComponent from '../lib/optionalComponent.js';
import IconButton from '@mui/material/IconButton';

import amazon from '../img/amazon.svg';
import netflix from '../img/netflix.png';
import disney from '../img/disney.png';

const mkImage = (provider) => {
  switch (provider) {
    case 'disneyplus':
      return (
        <img
          src={disney}
          style={{ width: 20, height: 11 }}
          aria-label="Disney+ Link"
        />
      );
    case 'netflix':
      return (
        <img
          src={netflix}
          style={{ width: 11, height: 20 }}
          aria-label="Netflix Link"
        />
      );
    case 'prime':
      return (
        <img
          src={amazon}
          style={{ width: 20, height: 20 }}
          aria-label="Amazon Prime Link"
        />
      );
    default:
      return <span>?</span>;
  }
};

export default function WatchMovieLink(props) {
  return (
    <div>
      {props.availability.map(({ link, provider }) => (
        <IconButton key={link} href={link} target="_blank">
          {mkImage(provider)}
        </IconButton>
      ))}
      {optionalComponent(
        props.availability.length === 0,
        <div style={{ marginTop: 15 }} />,
      )}
    </div>
  );
}

WatchMovieLink.propTypes = {
  availability: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.shape({
        link: PropTypes.string.isRequired,
        provider: PropTypes.string.isRequired,
      }),
    ),
  ).isRequired,
};
