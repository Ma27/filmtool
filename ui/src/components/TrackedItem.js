import * as React from 'react';
import PropTypes from 'prop-types';

import optionalComponent from '../lib/optionalComponent.js';

import moment from 'moment';

import Confirm from './Confirm.js';

import CircularProgress from '@mui/material/CircularProgress';
import DeleteIcon from '@mui/icons-material/Delete';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormLabel from '@mui/material/FormLabel';
import IconButton from '@mui/material/IconButton';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import Stack from '@mui/material/Stack';
import Switch from '@mui/material/Switch';

import rankToColor from '../lib/rank-to-color.js';

import WatchMovieLink from './WatchMovieLink.js';

let PREFIX;
if (process.env.NODE_ENV === 'development') {
  PREFIX = `http://localhost:${process.env.PORT || 5000}`;
} else {
  PREFIX = '/api';
}
export default function TrackedItem(props) {
  const [confirmDelete, setConfirmDelete] = React.useState(false);

  const color = rankToColor(props.rank);
  const change = (_, newRank) => {
    props.rankHandler(props.id, newRank);
  };

  const markAsSeen = () => {
    props.seenHandler(props.id);
  };
  const deletionHandler = () => {
    props.deletionHandler(props.id);
    setConfirmDelete(false);
  };

  return (
    <ListItem alignItems="flex-start">
      {optionalComponent(
        confirmDelete,
        <Confirm
          open={confirmDelete}
          resource={`Movie ${props.title}`}
          handler={deletionHandler}
          closeHandler={() => setConfirmDelete(false)}
        />,
      )}
      <ListItemAvatar>
        <img
          alt={`${props.title}`}
          src={`${PREFIX}/movies/${props.id}/image`}
          style={{
            marginRight: 15,
            borderLeft: `15px solid ${color}`,
            height: 150,
            width: 'auto',
          }}
        />
      </ListItemAvatar>
      {optionalComponent(
        props.updating,
        <Stack alignItems="center" style={{ marginTop: 65 }}>
          <CircularProgress />
        </Stack>,
      )}
      {optionalComponent(
        !props.updating,
        <React.Fragment>
          <ListItemText
            primary={
              <React.Fragment>
                {`${props.title} `}(
                <a
                  href={`https://www.themoviedb.org/movie/${props.tmdb_id}/`}
                  target="_blank"
                  rel="noreferrer"
                >
                  TMDb
                </a>
                )
              </React.Fragment>
            }
            secondaryTypographyProps={{ component: 'div' }}
            secondary={
              <React.Fragment>
                {props.description}
                <em>&nbsp;&ndash; added {moment(props.added).fromNow()}</em>
                <WatchMovieLink availability={props.availability} />
                <div>
                  <FormControl>
                    <FormLabel>Priority</FormLabel>
                    <RadioGroup
                      style={{ display: 'inline-block' }}
                      onChange={change}
                      value={props.rank}
                    >
                      <FormControlLabel
                        value="1"
                        control={
                          <Radio
                            checked={props.rank === 1}
                            sx={{
                              color: '#ff7066',
                              '&.Mui-checked': { color: '#ff7066' },
                            }}
                          />
                        }
                      />
                      <FormControlLabel
                        value="2"
                        control={
                          <Radio
                            checked={props.rank === 2}
                            sx={{
                              color: '#ff9a14',
                              '&.Mui-checked': { color: '#ff9a14' },
                            }}
                          />
                        }
                      />
                      <FormControlLabel
                        value="3"
                        control={
                          <Radio
                            checked={props.rank === 3}
                            sx={{
                              color: '#5297ff',
                              '&.Mui-checked': { color: '#5297ff' },
                            }}
                          />
                        }
                      />
                    </RadioGroup>
                  </FormControl>
                </div>
              </React.Fragment>
            }
          />
          <ListItemAvatar style={{ marginTop: 10 }}>
            <Switch edge="end" onChange={markAsSeen} />
          </ListItemAvatar>
          <ListItemAvatar style={{ paddingLeft: 10 }}>
            <IconButton
              edge="end"
              aria-label="delete"
              onClick={() => setConfirmDelete(true)}
            >
              <DeleteIcon />
            </IconButton>
          </ListItemAvatar>
        </React.Fragment>,
      )}
    </ListItem>
  );
}

TrackedItem.propTypes = {
  id: PropTypes.number.isRequired,
  tmdb_id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  added: PropTypes.string.isRequired,
  rank: PropTypes.number.isRequired,
  availability: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.shape({
        link: PropTypes.string.isRequired,
        provider: PropTypes.string.isRequired,
      }),
    ),
  ).isRequired,
  description: PropTypes.string.isRequired,
  rankHandler: PropTypes.func.isRequired,
  updating: PropTypes.bool.isRequired,
  seenHandler: PropTypes.func.isRequired,
  deletionHandler: PropTypes.func.isRequired,
};
