import * as React from 'react';

import { LayoutContainer, LayoutFragment } from './LayoutContainer.js';

import AddNew from './AddNew.js';
import Archive from './Archive.js';
import Overview from './Overview.js';
import TrashBin from './TrashBin.js';

import Box from '@mui/material/Box';
import Container from '@mui/material/Container';

export default function App() {
  return (
    <Container maxWidth="md">
      <Box sx={{ width: '100%', bgcolor: 'background.paper' }}>
        <LayoutContainer>
          <LayoutFragment title="Queued">
            <Overview />
          </LayoutFragment>
          <LayoutFragment title="Add new">
            <AddNew />
          </LayoutFragment>
          <LayoutFragment title="Archive">
            <Archive />
          </LayoutFragment>
          <LayoutFragment title="Trash bin">
            <TrashBin />
          </LayoutFragment>
        </LayoutContainer>
      </Box>
    </Container>
  );
}
