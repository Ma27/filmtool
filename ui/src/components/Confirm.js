import * as React from 'react';
import PropTypes from 'prop-types';

import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogTitle from '@mui/material/DialogTitle';

export default function Confirm(props) {
  const [open, setOpen] = React.useState(props.open);
  const action = () => {
    setOpen(false);
    props.handler();
  };
  const noAction = () => {
    setOpen(false);
    props.closeHandler();
  };

  return (
    <Dialog open={open} onClose={noAction}>
      <DialogTitle>Really delete {props.resource}?</DialogTitle>
      <DialogActions>
        <Button onClick={action} autoFocus color="success" variant="contained">
          Yes
        </Button>
        <Button onClick={noAction} color="error" variant="outlined">
          No
        </Button>
      </DialogActions>
    </Dialog>
  );
}

Confirm.propTypes = {
  open: PropTypes.bool.isRequired,
  resource: PropTypes.string.isRequired,
  handler: PropTypes.func.isRequired,
  closeHandler: PropTypes.func.isRequired,
};
