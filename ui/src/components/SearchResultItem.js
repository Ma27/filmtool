import * as React from 'react';
import PropTypes from 'prop-types';

import AddIcon from '@mui/icons-material/Add';
import IconButton from '@mui/material/IconButton';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';

export default function SearchResultItem(props) {
  const handle = () => {
    props.handleAdd({
      tmdb_id: props.tmdb_id,
    });
  };
  return (
    <ListItem alignItems="flex-start">
      <ListItemButton disableRipple={true}>
        <ListItemIcon onClick={handle}>
          <IconButton>
            <AddIcon />
          </IconButton>
        </ListItemIcon>
        <ListItemText
          primary={
            <React.Fragment>
              {`${props.original_title} `}(<a href={props.link}>TMDb</a>)
            </React.Fragment>
          }
          secondary={props.description}
        />
      </ListItemButton>
    </ListItem>
  );
}

SearchResultItem.propTypes = {
  tmdb_id: PropTypes.number.isRequired,
  original_title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  handleAdd: PropTypes.func.isRequired,
};
