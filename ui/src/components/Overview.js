import * as React from 'react';

import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {
  fetchTracked,
  updateRank,
  seen,
  unsee,
  rmMovie,
  addMovie,
  filter,
} from '../actions';
import optionalComponent from '../lib/optionalComponent.js';

import ResultList from './ResultList.js';
import TrackedItem from './TrackedItem.js';

import Alert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';

import { enqueueSnackbar, closeSnackbar } from 'notistack';
import buildUndoEnv from '../lib/build-undo-env.js';

class Overview extends React.Component {
  static propTypes = {
    result: PropTypes.array.isRequired,
    fetching: PropTypes.bool.isRequired,
    error: PropTypes.string,
    dispatch: PropTypes.func.isRequired,
    seenMovie: PropTypes.object,
    deletedMovie: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.inputRef = React.createRef();
  }

  componentDidMount() {
    if (this.props.result.length === 0) {
      this.props.dispatch(fetchTracked);
    }
  }

  newRankHandler = (id, rank) => {
    this.props.dispatch(updateRank(rank, id));
  };

  seenHandler = (id) => {
    this.props.dispatch(seen(id));
  };

  deletionHandler = (id) => {
    this.props.dispatch(rmMovie(id));
  };

  undoSeenHandler = (data) => {
    this.props.dispatch(unsee(data));
  };

  undoDeletionHandler = (data) => {
    this.props.dispatch(
      addMovie({
        tmdb_id: data.tmdb_id,
      }),
    );
  };

  filterMoviesHandler = () => {
    this.props.dispatch(filter(this.inputRef.current.value));
  };

  componentDidUpdate(pProps) {
    const close = closeSnackbar;
    if (this.props.seenMovie !== null && pProps.seenMovie === null) {
      const undo = (key) => {
        close(key);
        this.undoSeenHandler(this.props.seenMovie);
      };
      enqueueSnackbar(
        `Marked movie ${this.props.seenMovie.title} as seen`,
        buildUndoEnv(`seen-${this.props.seenMovie.id}`, close, undo),
      );
    }
    if (this.props.deletedMovie !== null && pProps.deletedMovie === null) {
      const undo = (key) => {
        close(key);
        this.undoDeletionHandler(this.props.deletedMovie);
      };
      enqueueSnackbar(
        `Removed ${this.props.deletedMovie.title} from watchlist`,
        buildUndoEnv(`rm-${this.props.deletedMovie.id}`, close, undo),
      );
    }
  }

  render() {
    return (
      <React.Fragment>
        <Box component="form">
          <FormControl fullWidth style={{ marginTop: 10 }}>
            <TextField
              fullWidth
              id="outlined-basic"
              label="Search"
              variant="outlined"
              inputRef={this.inputRef}
              onChange={this.filterMoviesHandler.bind(this)}
            />
          </FormControl>
        </Box>
        {optionalComponent(this.props.error !== null, () => (
          <Alert severity="error">{this.props.error}</Alert>
        ))}
        {optionalComponent(this.props.error === null, () => (
          <ResultList fetching={this.props.fetching}>
            {this.props.result
              .filter(({ show }) => show)
              .map((data, i) => (
                <TrackedItem
                  {...data}
                  rankHandler={this.newRankHandler.bind(this)}
                  seenHandler={this.seenHandler.bind(this)}
                  deletionHandler={this.deletionHandler.bind(this)}
                  key={i}
                />
              ))}
          </ResultList>
        ))}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => state.overview;

export default connect(mapStateToProps)(Overview);
