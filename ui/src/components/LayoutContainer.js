import * as React from 'react';
import PropTypes from 'prop-types';

import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

const allyProps = (index) => ({
  id: `simple-tab-${index}`,
  'aria-controls': `simple-tabpanel-${index}`,
});

export function LayoutContainer(props) {
  const [value, setValue] = React.useState(0);
  const handleChange = (_, newValue) => {
    setValue(newValue);
  };

  return (
    <React.Fragment>
      <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
        >
          {React.Children.map(props.children, (item, i) => (
            <Tab label={item.props.title} {...allyProps(i)} />
          ))}
        </Tabs>
      </Box>
      {React.Children.map(props.children, (item, i) => {
        return (
          <div {...allyProps(i)} hidden={value !== i}>
            {item}
          </div>
        );
      })}
    </React.Fragment>
  );
}

export function LayoutFragment(props) {
  return <React.Fragment>{props.children}</React.Fragment>;
}

LayoutFragment.propTypes = {
  title: PropTypes.string,
  children: PropTypes.any,
};

LayoutContainer.propTypes = {
  children: PropTypes.any,
};
